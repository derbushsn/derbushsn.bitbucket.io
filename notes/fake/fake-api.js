Fake.api = [
  {
    type: 'GET',
    url: '/api/sections',
    fake: true,
    fakeResult: function() {
      return {response: {data: Fake.db.findAll('sections')}};
    }
  },
  {
    type: 'GET',
    url: '/api/sections/[0-9]+',
    fake: true,
    fakeResult: function(options) {
      var id = options.url.replace('/api/sections/', '');
      var data = Fake.db.find('sections', {id: Number(id)});

      return {
        status: data ? 200 : 404,
        response: data ? {data: data} : {errors: [{message: "Раздел не найден"}]}
      };
    }
  },
  {
    type: 'GET',
    url: '/api/sections/[0-9]+/notes',
    fake: true,
    fakeResult: function(options) {
      var id = options.url.replace('/api/sections/', '').replace('/notes', '');
      var data = Fake.db.findAll('notes', {sectionId: Number(id)});

      data = Fake.db.extendRelation('author', data, 'authorId', 'users', 'id');

      return {
        status: 200,
        response: {data: data}
      };
    }
  },
  {
    type: 'GET',
    url: '/api/notes/[0-9]+',
    fake: true,
    fakeResult: function(options, headers) {
      var id = options.url.replace('/api/notes/', '');
      var data = Fake.db.find('notes', {id: Number(id)});
      data = Fake.db.extendRelation('author', data, 'authorId', 'users', 'id');
      return {
        status: data ? 200 : 404,
        response: data ? {data:data} : {errors: [{message: "Раздел не найден"}]}
      };
    }
  },
  {
    type: 'GET',
    url: '/api/notes/[0-9]+/comments',
    fake: true,
    fakeResult: function(options) {
      var id = options.url.replace('/api/notes/', '').replace('/comments', '');
      var data = Fake.db.findAll('comments', {noteId: Number(id)});

      data = Fake.db.extendRelation('author', data, 'authorId', 'users', 'id');

      return {
        status: 200,
        response: {data: data}
      };
    }
  },
  {
    type: 'GET',
    url: '/api/account',
    fake: true,
    fakeResult: function(options, headers) {
      var data = headers.token ? Fake.db.find('token', {token: headers.token}) : null;
      var user = data ? Fake.db.find('users', {id: data.user}) : null;

      return {
        status: user ? 200 : 401,
        response: user ? {data: _.omit(user, 'password')} : {errors: [{message: "User not auth"}]}
      };
    }
  },
  {
    type: 'DELETE',
    url: '/api/session',
    fake: true,
    fakeResult: function(options, headers) {
      var token = headers.token || null;

      if (token) {
        Fake.db.delete('token', {token: token});
      }

      return {
        status:  204,
        response: ''
      };
    }
  },
  {
    type: 'POST',
    url: '/api/session',
    fake: true,
    fakeResult: function(options, headers) {
      var hasError = false;
      var user;

      try {
        var data = JSON.parse(options.data || "{}");
      } catch(e) {
        hasError = true;
      }

      if (hasError || !data || !data.login || !data.password) {
        hasError = true;
      } else {
        user = Fake.db.find('users', {login: data.login});
        hasError = !user || user.password !== data.password;
      }

      if (hasError) {
        return {
          status: 400,
          response: {errors: [{message: "Некоректный логин или пароль"}]}
        }
      }

      var token = 'fake_token_' + user.login;
      if (!Fake.db.find('token', {token: token})) {
        Fake.db.insert('token', {user: user.id, token: token});
      }

      return {
        status:  200,
        response: {token: token}
      };
    }
  },
  {
    type: 'POST',
    url: '/api/comments',
    fake: true,
    fakeResult: function(options, headers) {
      var token = headers.token ? Fake.db.find('token', {token: headers.token}) : null;
      var user = token ? Fake.db.find('users', {id: token.user}) : null;
      var hasError = false;

      if (!user) {
        return {
          status: 403,
          response: {errors: [{message: "Операция не разрешена"}]}
        };
      }

      try {
        var data = JSON.parse(options.data || "");

        if (!data || !data.body || !data.noteId) {
          throw new Error("Неправильный формам данных");
        }

        var note = Fake.db.find('notes', {id: Number(data.noteId)});

        if (!note) {
          throw new Error("Неправильный формам данных");
        }
        var commentId = Fake.db.getNextId('comments');

        Fake.db.insert('comments', {
          id: commentId,
          body: data.body,
          noteId: note.id,
          authorId: user.id,
          revisionId: note.revisionId,
          created: moment.utc().format()
        });

      } catch(e) {
        return {
          status: 400,
          response: {errors: [{message: "Произошла ошибка"}]}
        };
      }

      return {
        status: 200,
        response: Fake.db.find('comments', {id: commentId})
      };
    }
  },
  {
    type: 'POST',
    url: '/api/account',
    fake: true,
    fakeResult: function(options, headers) {
      var hasError = false;
      try {
        var data = JSON.parse(options.data || "{}");
      } catch(e) {
        hasError = true;
      }

      if (hasError || !data || !data.login) {
        return {
          status: 400,
          response: {errors: [{field: 'login', message: "Логин не может быть пустым"}]}
        }
      }

      if (hasError || !data || !data.password) {
        return {
          status: 400,
          response: {errors: [{field: 'password', message: "Пароль не может быть пустым"}]}
        }
      }

      if (!hasError) {
        var userData = {
          firstName: data.firstName || '',
          lastName: data.lastName || '',
          patronymic: data.patronymic || '',
          login: data.login,
          password: data.password
        };

        if (Fake.db.find('users', {login: userData.login})) {
          return {
            status: 400,
            response: {errors: [{field: 'login', message: "Пользователь с таким логином существует"}]}
          }
        }

        userData.id = Fake.db.getNextId('users');
        Fake.db.insert('users', userData);
      }

      if (hasError) {
        return {
          status: 400,
          response: {errors: [{message: "Ошибка в данных"}]}
        }
      }

      var token = 'fake_token_' + userData.login;
      if (!Fake.db.find('token', {token: token})) {
        Fake.db.insert('token', {user: userData.id, token: token});
      }

      return {
        status:  200,
        response: {data: _.omit(userData, 'password'), token: token}
      };
    }
  },
  {
    type: 'DELETE',
    url: '/api/comments/[0-9]+',
    fake: true,
    fakeResult: function(options, headers) {
      var id = Number(options.url.replace('/api/comments/', ''));
      var token = headers.token ? Fake.db.find('token', {token: headers.token}) : null;
      var user = token ? Fake.db.find('users', {id: token.user}) : null;

      if (!user) {
        return {
          status: 403,
          response: {errors: [{message: "Операция не разрешена"}]}
        };
      }
      Fake.db.delete('comments', {id: id, authorId: user.id});

      return {
        status:  204,
        response: ''
      };
    }
  },
  {
    type: 'POST',
    url: '/api/notes',
    fake: true,
    fakeResult: function(options, headers) {
      var token = headers.token ? Fake.db.find('token', {token: headers.token}) : null;
      var user = token ? Fake.db.find('users', {id: token.user}) : null;

      if (!user) {
        return {
          status: 403,
          response: {errors: [{message: "Операция не разрешена"}]}
        };
      }

      try {
        var data = JSON.parse(options.data || "");

        if (!data || !data.body || !data.subject || !data.sectionId) {
          throw new Error("Неправильный формам данных");
        }

        var section = Fake.db.find('sections', {id: data.sectionId});
        if (!section) {
          throw new Error("Секция не задана");
        }

        var created = moment.utc().format();
        var revisionId = Fake.db.getNextId('revisions');
        var revisionData = {
          id: revisionId,
          created: created,
          body: data.body
        };

        console.log(revisionData);
        Fake.db.insert('revisions', revisionData);

        var noteId = Fake.db.getNextId('notes');

        var noteData = {
          id: noteId,
          subject: data.subject,
          body: data.body,
          sectionId: section.id,
          authorId: user.id,
          created: created,
          revisionId: revisionData.id
        };

        console.log(noteData);
        Fake.db.insert('notes', noteData);

      } catch(e) {
        return {
          status: 400,
          response: {errors: [{message: "Произошла ошибка"}]}
        };
      }

      return {
        status: 200,
        response: Fake.db.find('notes', {id: noteId})
      };
    }
  },
];