var Fake = function(api) {
  this.api = api;
  this.init();
};
$.extend(Fake.prototype, {
  getFake: function(options, originalOptions, jqXHR) {
    return _.find(this.api, function(item)  {
      return item.fake && item.fakeResult && this.isEqualPattern(item, options) && options.type.toLowerCase() == item.type.toLowerCase();
    }.bind(this));
  },
  isEqualPattern: function(item, options) {
    return (new RegExp('^' + item.url + '$', 'i')).test(options.url);
  },
  log: function(data) {
    console.debug(data);
  },
  init: function() {
    $.ajaxTransport("json", function(options, originalOptions, jqXHR) {
      var api = this.getFake(options, originalOptions, jqXHR);

      if (api) {
        return {
          send: function(headers, completeCallback) {
            var r = typeof api.fakeResult == "function" ? api.fakeResult(options, headers) : api.fakeResult;
            r.headers = r.headers || [];
            r.status = r.status || 200;
            r.statusText = r.status >= 200 && r.status < 300 ? 'success' : 'error';
            try {
              this.log("\n\nRequest: \n" + options.type + ': ' + options.url);
              this.log("Headers: \n" + JSON.stringify(headers) + "\n");
              if (options.data) {
                this.log("Data: \n" + options.data + "\n");
              }

              this.log("\nResponse: \n");
              this.log("Status: " + r.status + " (" + r.statusText + ")" + "\n");
              this.log(JSON.stringify(r.response || "") + "\n");
            } catch (e) {
              console.log(e);
            }

            completeCallback(r.status, r.statusText, {"json": r.response}, r.headers);
          }.bind(this),
          abort: function() {
            console.log('abort');
          }
        };
      }
    }.bind(this));

  }
});