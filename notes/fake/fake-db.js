Fake.db = {
  increments: {},
  insert: function(table, data) {
    if (!_.has(this.data, table)) {
        this.data[table] = [];
        this.increments[table] = 0;
    }
    if (_.isArray(data)) {
      Array.prototype.push.apply(this.data[table], data);
    } else {
      this.data[table].push(data);
    }

    this.updateIncrement(table);
  },
  updateIncrement: function(table) {
    var obj = _.max(this.data[table] || {id: 0}, function(item) {
      return item.id;
    });
    var computedIncrement = (obj ? obj.id : 0) + 1;
    this.increments[table] = Math.max(computedIncrement, this.increments[table] + 1);
  },

  update: function(table, data, where) {
    if (!_.has(this.data, table)) {
      return false;
    }

    var entry = _.findWhere(this.data[table], where);
    if (entry) {
      _.extend(entry, data);
      return true;
    }

    return false;
  },

  find: function(table, where) {
    return _.findWhere(this.data[table], where);
  },

  findAll: function(table, where) {
    return _.where(this.data[table], where);
  },

  data: {},
  extendRelation: function(field, data, key, target, keyTarget) {
    var where = {};
    var result = {};
    if (_.isArray(data)) {
      return _.map(data, function(item) {
        return this.extendRelation(field, item, key, target, keyTarget);
      }.bind(this))
    } else {
      where[keyTarget] = data[key];
      result[field] = this.find(target, where);
      return $.extend({}, data, result);
    }
  },
  delete: function(table, where) {
    this.data[table] = _.filter(this.data[table], function(item) {
      return !_.findWhere([item], where);
    }.bind(this));
  },

  getNextId: function(table) {
    if (!this.increments[table]) {
      this.increments[table] = 0;
    }

    this.increments[table]++;

    return this.increments[table];
  }
};

Fake.db.insert('sections', [
  {id: 1, name: 'Фильмы и сериалы'},
  {id: 2, name: 'Про людей'}
]);

Fake.db.insert('notes', [
  {
    id: 1,
    subject: "Интересные факты о фильме «Кавказская пленница»",
    body: "Не сразу была утверждена песня о белых медведях. В первоначальном варианте в ней были слова: «чешут медведи спину о земную ось». В Худсовете возмутились: «Почему они чешутся? Их что – блохи кусают?». Пришлось поэту Леониду Дербеневу переделать текст. Получилось: «трутся спиной медведи».",
    sectionId: 1,
    authorId: 1,
    created: "2017-01-29T17:22:27.000Z",
    revisionId: 1
  },
  {
    id: 2,
    subject: "Факты из сериала Теория большого взрыва",
    body: "<ul><li>Режиссер планировать назвать главных героев созвучными именами – Лени и Кенни, но передумал. Главных персонажей сериала решили назвать в честь именитого продюсера Леонарда Шелдона. Именно поэтому героев зовут Шелдон и Леонард.</li><li>Когда главные герои поднимаются вверх по лестнице, то мы видим, что они проходят несколько этажей. На самом деле дом одноэтажный и персонажам приходится проходить одну и ту же лестницу по несколько раз.</li></ul>",
    sectionId: 1,
    authorId: 1,
    created: "2017-01-29T17:22:27.000Z",
    revisionId: 1
  },
  {
    id: 3,
    subject: "Почему люди зевают",
    body: "Зевота - это рефлекторный глубокий вдох, по мнению ученых сигнализирующий о скуке, усталости, недостатке кислорода в организме или даже болезни.",
    sectionId: 2,
    authorId: 2,
    created: "2017-01-29T17:22:27.000Z",
    revisionId: 1
  }
]);

Fake.db.insert('revisions', [
  {
    id: 1,
    created: "2017-01-29T17:22:27.000Z",
    body: "Не сразу была утверждена песня о белых медведях. В первоначальном варианте в ней были слова: «чешут медведи спину о земную ось». В Худсовете возмутились: «Почему они чешутся? Их что – блохи кусают?». Пришлось поэту Леониду Дербеневу переделать текст. Получилось: «трутся спиной медведи»."
  }
]);

Fake.db.insert('comments', [
  {
    id: 1,
    body: "Мало фактов!!!",
    authorId: 1,
    noteId: 1,
    revisionId: 1,
    created: "2017-01-29T17:22:27.000Z"
  },
  {
    id: 2,
    body: "Надо дополнить",
    authorId: 2,
    noteId: 1,
    revisionId: 1,
    created: "2017-01-29T17:22:27.000Z"
  },
  {
    id: 3,
    body: "Забавно",
    authorId: 2,
    noteId: 2,
    revisionId: 1,
    created: "2017-01-29T17:22:27.000Z"
  }
]);

Fake.db.insert('users', [
  {
    id: 1,
    firstName: "имя",
    lastName: "фамилия",
    patronymic: "отчество",
    login: "admin",
    password: "admin"
  },
  {
    id: 2,
    firstName: "имя",
    lastName: "фамилия",
    patronymic: "отчество",
    login: "user",
    password: "user"
  }
]);

Fake.db.insert('token', [{user: 1, token: 'fake_token_admin'}]);
